const request = require('request');

const Api = async (data) => {

      return new Promise(function (resolve, reject) {
        request(data, function (error, res, body) {
          if (!error && res.statusCode == 200) {
            resolve(body);
          } else {
            reject(error);
          }
        });
      });
  
};

module.exports = Api;