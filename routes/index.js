const express = require("express");
const router = express.Router();
const getResults = require("../scraper");
const Api = require("../api");
const url = 'https://fabelio.com/insider/data/product/id/';
const url_more_image = 'https://fabelio.com/swatches/ajax/media/?product_id=';
const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')
const adapter = new FileSync('db.json')
const db = low(adapter)

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}
// Set some defaults (required if your JSON file is empty)
db.defaults({ data: [], user: {}, count: 0 })
  .write()


/* GET home page. */
router.get("/", async function(req, res, next) {
  if (req.query !== {}){
    res.render("index",{id : req.query.id });
  } else{
    res.render("index");
  }
});

/* GET home page. */
router.get("/history", async function(req, res, next) {
  // console.log(db.get('data').value())
  res.render("history",{rows:db.get('data').value()});
});

/* GET home page. */
router.get("/api", async function(req, res, next) {
  const result = await getResults(req.query.url);
  const response_api = await Api(url+result.positions);
  // Add a post
  db.get('data')
  .push({'data' : JSON.parse(response_api) , 'id' : result.positions,'uuid' : uuidv4()})
  .write()
  res.status(200).send({'data' : JSON.parse(response_api) , 'id' : result.positions})
});

/* GET home page. */
router.get("/api_database", async function(req, res, next) {
  let data = db.get('data').find({ uuid: req.query.uuid }).value()
  res.status(200).send({data , 'id' : data.id})
});



/* GET home page. */
router.get("/more_image", async function(req, res, next) {
  const response_api = await Api(url_more_image+req.query.product_id);
  res.status(200).send(JSON.parse(response_api))
});

module.exports = router;
