const cheerio = require("cheerio");
const axios = require("axios");

// const siteUrl = "https://fabelio.com/ip/kursi-makan-emma-accent.html";

const fetchData = async (siteUrl) => {
  const result = await axios.get(siteUrl);
  return cheerio.load(result.data);
};

const getResults = async (siteUrl) => {
  const $ = await fetchData(siteUrl);

  return {
    positions: $('input[name="product"]').val(),
  };
};

module.exports = getResults;
